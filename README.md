# aoc2022

[![Package Version](https://img.shields.io/hexpm/v/aoc2022)](https://hex.pm/packages/aoc2022)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/aoc2022/)

A Gleam project

## Quick start

```sh
gleam run   # Run the project
gleam test  # Run the tests
gleam shell # Run an Erlang shell
```

## Installation

If available on Hex this package can be added to your Gleam project:

```sh
gleam add aoc2022
```

and its documentation can be found at <https://hexdocs.pm/aoc2022>.
