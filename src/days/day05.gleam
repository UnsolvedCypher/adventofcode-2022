import gleam/io
import utils/parse
import gleam/regex
import gleam/int
import gleam/string
import gleam/list
import unwrap.{unwrap}

type Stacks = List(List(String))

type Move {
  Move(how_many: Int, source: Int, target: Int)
}

fn update_stacks_at_index(stacks: Stacks, new_value: List(String), index: Int) {
  stacks
  |> list.index_map(fn (curr_index: Int, original_value: List(String)) {
    case index == curr_index {
      True -> new_value
      False-> original_value
    }
  })
}

fn make_single_move(stacks: Stacks, source: Int, target: Int) {
  let source_stack = list.at(stacks, source - 1)
  |> unwrap
  let target_stack = list.at(stacks, target - 1)
  |> unwrap
  let [item_to_move, ..new_source] = source_stack
  let new_target = [item_to_move, ..target_stack]

  stacks
  |> update_stacks_at_index(new_target, target - 1)
  |> update_stacks_at_index(new_source, source - 1)
}

fn make_stack_move(stacks: Stacks, move: Move) -> Stacks {
  case move.how_many {
    0 -> stacks
    _ -> {
      let moved_stacks = make_single_move(stacks, move.source, move.target)
      let remaining_move = Move(..move, how_many: move.how_many - 1)
      make_stack_move(moved_stacks, remaining_move)
    }
  }
}

fn make_part2_stack_move(stacks: Stacks, move: Move) -> Stacks {
  let source_stack = list.at(stacks, move.source - 1)
  |> unwrap
  let target_stack = list.at(stacks, move.target - 1)
  |> unwrap
  let #(items_to_move, new_source) = list.split(source_stack, move.how_many)
  let new_target = list.append(items_to_move, target_stack)

   stacks
  |> update_stacks_at_index(new_target, move.target - 1)
  |> update_stacks_at_index(new_source, move.source - 1)
}

fn line_to_items(line: String) {
  "\\w|\\s{4}"
  |> regex.from_string
  |> unwrap
  |> regex.scan(line)
  |> list.map(fn(match) {match.content})
}

fn line_to_move(line: String) {
  let [how_many, source, target] = "\\d+"
  |> regex.from_string
  |> unwrap
  |> regex.scan(line)
  |> list.map(fn(match) {match.content})
  |> list.map(int.parse)
  |> list.map(unwrap)

  Move(how_many, source, target)
}

fn first_letters_of_stack(stacks: Stacks) {
  stacks
  |> list.map(list.first)
  |> list.map(unwrap)
  |> string.join("")
}

pub fn main() {
  let [stacks_input, moves_input] = parse.read_day(5)
  |> string.split("\n\n")
  
  let stacks = stacks_input
  |> string.split("\n")
  |> list.map(line_to_items)
  |> list.transpose
  |> list.map(list.filter(_, fn (item) {item != "    "}))
  
  let moves = moves_input
  |> string.trim
  |> string.split("\n")
  |> list.map(line_to_move)
  
  io.println("Part 1: ")
  moves
  |> list.fold(stacks, make_stack_move)
  |> first_letters_of_stack
  |> io.println

  io.println("Part 2: ")
  moves
  |> list.fold(stacks, make_part2_stack_move)
  |> first_letters_of_stack
  |> io.println
}
