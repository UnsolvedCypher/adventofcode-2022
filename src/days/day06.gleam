
import gleam/string
import gleam/bool
import gleam/int
import gleam/io
import gleam/list
import gleam/map
import gleam/set
import gleam/float
import unwrap.{unwrap}
import utils/parse
import gleam/bit_string
import days/day04

type LastChars {
  LastChars(chars: List(String), index: Int)
}

fn last_chars_unique(last_chars: LastChars) {
  let unique = last_chars.chars
  |> list.unique
  
  let complete = last_chars.chars
  |> list.contains("")
  |> bool.negate

  list.length(unique) == list.length(last_chars.chars) 
    && complete
  
}

fn add_char(last_chars: LastChars, new_char: String) {
  let [_, ..new_chars] = last_chars.chars
  let new_chars = new_chars
  |> list.append([new_char])
  
  LastChars(new_chars, last_chars.index + 1)
}

pub fn run() {
  let start = LastChars(list.repeat("", 14), 0)
  parse.read_day(6)
  |> string.trim
  |> string.to_graphemes
  |> list.fold_until(
    from: start,
    with: fn (last_chars, new_char) {
      let last_chars = add_char(last_chars, new_char)
      case last_chars_unique(last_chars) {
        True -> list.Stop(last_chars)
        False -> list.Continue(last_chars)
      }
    }
  )
  |> io.debug
  0
}
