import gleam/string
import gleam/int
import gleam/io
import gleam/list
import gleam/map
import gleam/set
import gleam/float
import unwrap.{unwrap}
import utils/parse
import gleam/bit_string

fn get_priority(letter: String) {
  case bit_string.from_string(letter) {
    <<x:8>> if x >= 97 -> x - 97 + 1
    <<x:8>> -> x - 65 + 27
  }
}

fn find_common_item(lists: List(List(a))) -> a {
  lists
  |> list.map(set.from_list)
  |> list.reduce(set.intersection)
  |> unwrap
  |> set.to_list
  |> list.first
  |> unwrap
}

fn duplicate_in_rucksack(rucksack: String) {
  rucksack
  |> string.to_graphemes
  |> list.sized_chunk(string.length(rucksack) / 2)
  |> find_common_item
}

fn duplicate_in_group(group: List(String)) {
  group
  |> list.map(string.to_graphemes)
  |> find_common_item
}

pub fn run() {
  io.println("Part 1:")
  parse.day_split_by(3, "\n")
  |> list.map(duplicate_in_rucksack)
  |> list.map(get_priority)
  |> int.sum
  |> int.to_string
  |> io.println
  
  io.println("Part 2:")
  parse.day_split_by(3, "\n")
  |> list.sized_chunk(3)
  |> list.map(duplicate_in_group)
  |> list.map(get_priority)
  |> int.sum
  |> int.to_string
  |> io.println
}
