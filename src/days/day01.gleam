import gleam/string
import gleam/int
import gleam/io
import gleam/list
import gleam/map
import gleam/set
import gleam/float
import unwrap.{unwrap}
import utils/parse

fn elf_totals() {
  parse.day_split_by(num: 1, by: "\n\n")
  |> list.map(fn(chunk) {
      chunk
      |> string.split("\n")
      |> list.map(int.parse)
      |> list.map(unwrap)
      |> int.sum
    })
  |> list.sort(int.compare)
  |> list.reverse
}

pub fn run() {
  let elf_totals = elf_totals()
  let part_1 = elf_totals
  |> list.first
  |> unwrap

  let part_2 = elf_totals
  |> list.take(3)
  |> int.sum

  io.println("Day 1: " <> int.to_string(part_1) <> ", part 2: " <> int.to_string(part_2))
}
