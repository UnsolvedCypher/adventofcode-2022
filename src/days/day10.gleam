import gleam/string
import gleam/int
import gleam/io
import gleam/list
import gleam/map
import gleam/set
import gleam/float
import unwrap.{unwrap}
import utils/parse
import gleam/bit_string

type Status {
  Add(at_cycle_num: Int, to_add: Int)
  Noop
}

fn sprite_visible_at_pixel(pixel_num: Int, register: Int) {
  io.println("Pixel num, register:")
  io.println(string.inspect(#(pixel_num, register)))
  let pixel_x = pixel_num % 40
  
  int.absolute_value(pixel_x - register) <= 1
}

fn process_line(instructions: List(String), status: Status, cycle_num: Int, register: Int, strengths: List(Int), display_output: String) {
 
  let register = case status {
    Add(at_cycle, to_add) if cycle_num == at_cycle -> register + to_add
    _ -> register
  }

  let display_line_break = {cycle_num} % 40 == 0

  let display_output = display_output
  |> string.append(
    case sprite_visible_at_pixel(cycle_num - 1, register) {
      True -> "#" 
      False -> "."
    }
  )
  |> string.append(
    case display_line_break {
      True -> "\n"
      False -> ""
    }
  )

  let is_interesting_cycle = { cycle_num - 20 } % 40 == 0
  let strengths = case cycle_num {
    x if is_interesting_cycle -> [x * register, ..strengths]
    _ -> strengths
  }
  
  case status, instructions {
    Add(at_cycle_num, _), _ if at_cycle_num != cycle_num -> process_line(
      instructions, 
      status, 
      cycle_num + 1, 
      register, 
      strengths,
      display_output)
    _, [instruction, ..rest] -> {
      let next_status = case string.split(instruction, " ", ) {
        ["noop"] -> Noop
        ["addx", x] -> Add(cycle_num + 2, unwrap(int.parse(x)))
      }
      process_line(rest, next_status, cycle_num + 1, register, strengths, display_output)
    }
    _, [] -> { // No more instructions left, we're done
      io.println(display_output)
      strengths
    }
  }
}

pub fn run() {
  parse.read_day(10)
  |> string.trim
  |> string.split("\n")
  |> list.map(string.trim)
  |> process_line(Noop, 1, 1, [], "")
  |> int.sum
  |> io.debug
}
