import gleam/string
import gleam/regex
import gleam/int
import gleam/io
import gleam/list
import gleam/map
import gleam/set
import gleam/float
import unwrap.{unwrap}
import utils/parse
import gleam/bit_string
import days/day03

pub fn pair_contained(pairs: List(Int)) {
  let [a, b, c, d] = pairs
  {a >= c && b <= d} || {a <= c && b >= d}
}

pub fn pairs_overlap(pairs: List(Int)) {
  let [a, b, c, d] = pairs
  {b >= c && b <= d} || {d >= a && d <= b}
}

pub fn run() {
  let parsed_input = parse.day_split_by(4, "\n")
  |> list.map(fn(line) {
    "-|,"
    |> regex.from_string
    |> unwrap
    |> regex.split(line)
    |> list.map(int.parse)
    |> list.map(unwrap)
  })

  io.println("Part 1:")

  parsed_input
  |> list.filter(pair_contained)
  |> list.length
  |> int.to_string
  |> io.println

  io.println("Part 2:")

  parsed_input
  |> list.filter(pairs_overlap)
  |> list.length
  |> int.to_string
  |> io.println
}
