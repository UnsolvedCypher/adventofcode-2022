import gleam/string
import gleam/option
import gleam/function
import gleam/regex.{Match}
import gleam/int
import gleam/io
import gleam/list
import gleam/map.{Map}
import gleam/set
import gleam/float
import unwrap.{unwrap}
import utils/parse
import gleam/bit_string

////////// Types //////////

type Monkey {
  Monkey(
    monkey_num: Int, 
    items: List(Int), 
    operation: fn(Int) -> Int, 
    test_divisible_by: Int, 
    true_monkey_num: Int, 
    false_monkey_num: Int,
    inspections: Int
  )
}

type Monkeys = Map(Int, Monkey)

////////// Math //////////

fn gcd(a: Int, b: Int) {
  case b == 0 {
    True -> a
    False -> gcd(b, a % b)
  }
}

fn lcm(a: Int, b: Int) {
  a * b / gcd(a, b)
}


fn monkey_lcm(monkeys: Monkeys) {
  monkeys
  |> map.values
  |> list.map(fn(monkey) {monkey.test_divisible_by})
  |> list.reduce(lcm)
  |> unwrap
}

////////// Input parsing //////////

fn string_after(str: String, substr: String) {
  let #(_, result) = str
  |> string.split_once(substr)
  |> unwrap

  result
}

fn extract_digits(str: String) {
  regex.from_string(".*?(\\d+)")
  |> unwrap
  |> regex.scan(str)
  |> list.at(0)
  |> unwrap
  |> fn (match: Match) { 
    match.submatches
    |> list.first
    |> unwrap
    |> option.to_result("")
    |> unwrap
    |> int.parse
    |> unwrap
  }
}

fn perform_operation(old_val: Int, operation_text: String) {
  let split_text = operation_text
  |> string.split(" ")
  
  let left = case split_text
  |> list.at(0)
  |> unwrap {
    "old" -> old_val
    n -> unwrap(int.parse(n))
  }
  
  let operator = case split_text
  |> list.at(1)
  |> unwrap {
    "+" -> int.add
    "-" -> int.subtract
    "*" -> int.multiply
    "/" -> fn (a: Int, b: Int) { a / b } // Divide returns a result rather than an int
  }

  let right = case split_text
  |> list.at(2)
  |> unwrap {
    "old" -> old_val
    n -> unwrap(int.parse(n))
  }
  
  operator(left, right)
}

fn parse_monkey_block(block: String) {
  let lines = block
  |> string.split("\n")
  
  let monkey_num = lines
  |> list.at(0)
  |> unwrap
  |> extract_digits

  
  let starting_items = lines
  |> list.at(1)
  |> unwrap
  |> string_after(": ")
  |> string.split(", ")
  |> list.map(int.parse)
  |> list.map(unwrap)
  
  let operation: fn(Int) -> Int = lines
  |> list.at(2)
  |> unwrap
  |> string_after(" = ")
  |> fn (operation_text: String) {
    perform_operation(_, operation_text)
  }
  
  let divisible_by = lines
  |> list.at(3)
  |> unwrap
  |> string_after("divisible by ")
  |> int.parse
  |> unwrap
  
  let if_true_monkey = lines
  |> list.at(4)
  |> unwrap
  |> extract_digits


  let if_false_monkey = lines
  |> list.at(5)
  |> unwrap
  |> extract_digits


  Monkey(
    monkey_num: monkey_num, 
    items: starting_items, 
    operation: operation, 
    test_divisible_by: divisible_by, 
    true_monkey_num: if_true_monkey, 
    false_monkey_num: if_false_monkey,
    inspections: 0
  )
}


////////// Monkey thowing //////////

fn throw(among monkeys: Monkeys, throw item: Int, to target: Int) {

  let target_monkey = monkeys
  |> map.get(target)
  |> unwrap
  
  let target_monkey = Monkey(
    ..target_monkey, 
    items: list.append(target_monkey.items, [item % monkey_lcm(monkeys)])
  )
  map.insert(into: monkeys, for: target, insert: target_monkey)
}

fn perform_monkey_turn(monkeys: Monkeys, monkey_num: Int) -> Monkeys {
  let curr_monkey = monkeys
  |> map.get(monkey_num)
  |> unwrap

  let items = curr_monkey.items
  let curr_monkey = Monkey(..curr_monkey, items: [], inspections: curr_monkey.inspections + list.length(curr_monkey.items))

  let monkeys = monkeys
  |> map.insert(monkey_num, curr_monkey)

  items
  |> list.fold(
    monkeys,
    fn(curr_monkeys: Monkeys, item: Int) {
      let item = curr_monkey.operation(item)// / 3
      case item % curr_monkey.test_divisible_by == 0 {
        True -> throw(among: curr_monkeys, throw: item, to: curr_monkey.true_monkey_num)
        False -> throw(among: curr_monkeys, throw: item, to: curr_monkey.false_monkey_num)
      }
    }
  )
}

fn perform_round(monkeys: Monkeys) -> Monkeys {
  list.range(0, map.size(monkeys) - 1)
  |> list.fold(
    monkeys,
    perform_monkey_turn
  )
}

pub fn run() {
  let monkeys = parse.read_day(11)
  |> string.trim
  |> string.split("\n\n")
  |> list.map(parse_monkey_block)
  |> list.map(
    fn(monkey: Monkey) { 
      #(monkey.monkey_num, monkey) 
    }
  )
  |> map.from_list
  

  let inspections = list.range(1, 10000)
  |> list.fold(monkeys, fn(monkeys, _) { perform_round(monkeys) })
  |> map.values
  |> list.map(fn(v) {v.inspections})
  |> list.sort(int.compare)
  |> list.reverse

  int.multiply(
    unwrap(list.at(inspections, 0)),
    unwrap(list.at(inspections, 1))
  )
  |> int.to_string
  |> io.println

}
