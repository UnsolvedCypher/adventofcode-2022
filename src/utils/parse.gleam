import gleam/int
import gleam/string
import gleam/erlang/file
import unwrap.{unwrap}

pub fn read_day(day: Int) {
  file.read("inputs/day" <> int.to_string(day) <> ".txt")
  |> unwrap
  |> string.trim
}

pub fn day_split_by(num input: Int, by separator: String) {
  read_day(input)
  |> string.split(separator)
}
