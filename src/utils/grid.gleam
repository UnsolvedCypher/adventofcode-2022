import gleam/list
import gleam/string
import gleam/result

pub opaque type Grid(a) {
  Grid(inner: List(List(a)))
}

pub fn from_input(from input: String, rows_by row_separator: String, cols_by col_separator: String) -> Grid(String) {
  let rows = string.split(input, row_separator)
  let inner = list.map(rows, string.split(_, col_separator))
  Grid(inner)
}

pub fn at(grid: Grid(a), x: Int, y: Int) -> Result(a, Nil) {
  grid.inner
  |> list.at(y)
  |> result.then(list.at(_, x))
}

pub fn indx_map(grid: Grid(a), mapper: fn(a, Int, Int) -> b) -> Grid(b) {
  grid.inner
  |> list.index_map(fn(y, row) {
    list.index_map(row, fn(x, elem) {
      mapper(elem, x, y)
    })
  })
  |> Grid
}

pub fn as_list(grid: Grid(a)) -> List(List(a)) {
  grid.inner
}
