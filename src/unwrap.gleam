pub fn unwrap(x: Result(a, b)) -> a {
  assert Ok(x) = x
  x
}
